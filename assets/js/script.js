"use strict";

let $obj = null;

$(document).ready(function () {
    console.log('Docment ready.');

    if ($(window).width() < 992) {
        $('.navbar-light .navbar-nav .nav-link').on('click', function (e) {
            $('#navbarSupportedContent').removeClass('show');
        });

        $('body').on('click', '#navbarSupportedContent .button-close', function (e) {
            $('#navbarSupportedContent').removeClass('show');
        });

        $('#navbarSupportedContent').append('<div class="button-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"/></svg></div>');

        $('.single-item').slick({
            infinite: true,
            dots: true,
        });

        $('#section-portfolio .hidden-image').removeClass('hidden-image');
    }
    ;

    $('#scroll-to-top').on('click', function (e) {
        e.preventDefault();

        $('html, body').animate({scrollTop: 0}, 100);
    });

    $('#mainmenu .navbar-toggler').on('click', function (e) {
        e.preventDefault();

        $('#navbarSupportedContent').toggleClass('show');
    });

    $('[data-target="blank"]').on('click', function (e) {
        e.preventDefault();

        window.open($(this).attr('href'), '_blank');
    });

    $('[href="#"][data-target-id]').on('click', function (e) {
        e.preventDefault();

        let id = $(this).attr('data-target-id');

        $obj = $('#' + id);

        window.scrollTo({
            top: $obj.offset().top - 100,
            behavior: "smooth"
        });
    });

    $('[data-function="show-hidden"]').on('click', function (e) {
        e.preventDefault();

        let targetClass = $(this).attr('data-target-class');

        if ($obj = document.querySelectorAll('.' + targetClass)) {
            $obj.forEach((entry) => {
                entry.classList.remove('hidden-image');
            });
        }
        ;

        this.remove();
    });

    $('body').on('click', '.privacy-agreement.show-popover', function(e) {
        e.preventDefault();

        Swal.fire({
            html: $('#settings [name="privacy_policy_text"]').val(),
            showCancelButton: false,
            showConfirmButton: true,
            showCloseButton: true

        });
    });

    $('body').on('submit', '.callback-form', function(e) {
        e.preventDefault();

        let $form = $(this);
        let capturePoint = 'Контакты внизу сайта';
        let mailTo = $('#settings [name="mailto"]').val();

        if (!mailTo)  {
            Swal.fire({
                title: 'Ошибка!',
                text: 'Что-то пошло не так... Не найден e-mail администратора...',
                showCancelButton: false,
                showConfirmButton: true,
                showCloseButton: true
            });

            exit;
        };

        if ($form.find('[name="capture-point"]').length) capturePoint = $form.find('[name="capture-point"]').val();

        let formData = {
            'mailto'        : mailTo,
            'name'          : $form.find('[name="name"]').val(),
            'phone'         : $form.find('[name="phone"]').val(),
            'email'         : $form.find('[name="email"]').val(),
            'capturePoint'  : capturePoint
        };

        $.ajax({
            url: `${location.origin}/api/processOrder.php`,
            async:false,
            type: 'POST',
            data: formData,
            dataType: "html",
            // encode: true,
            beforeSend: function() {},
            success:  function(data) {
                console.log(data);

                Swal.fire({
                    title: 'Спасибо за заявку!',
                    text: 'Мы свяжемся с Вами в ближайшее время!',
                    showCancelButton: false,
                    showConfirmButton: true,
                    showCloseButton: true
                });
            },
            fail: function(xhr, textStatus, errorThrown) {
                console.error('Что-то пошло не так... Не могу добавить заявку в базу данных: '+xhr+';'+textStatus+';'+errorThrown);

                Swal.fire({
                    title: 'Ошибка!',
                    text: 'Что-то пошло не так... Не могу добавить заявку в базу данных...',
                    showCancelButton: false,
                    showConfirmButton: true,
                    showCloseButton: true
                });
            }
        });

    });

    $('.section-stages-item').on('click', function (e) {
        e.preventDefault();

        $('.section-stages-item').removeClass('active');
        $(this).addClass('active');

        let currentTitle = $(this).find('.title').text();
        let currentText = $(this).find('.text').text();

        $('.section-stages-selected-text .title').text(currentTitle);
        $('.section-stages-selected-text .text').text(currentText);
    });

    $('.section-faq-item-question').on('click', function (e) {
        e.preventDefault();

        let $section_faq_item = $(this).closest('.section-faq-item');

        $section_faq_item.toggleClass('opened');
    });

    $('.make-order.show-popover').on('click', function (e) {
        let formHTML = $('#callback-form').html();
        let capturePint = $(this).closest('.inner').find('[data-capture-point]').attr('data-capture-point');

        Swal.fire({
            html: `<form action="#" id="callback-form" class="callback-form"><input type="hidden" name="capture-point" value="${capturePint}">` + formHTML + `</form>`,
            showCancelButton: false,
            showConfirmButton: false,
            showCloseButton: true
        });
    });
});

$(window).scroll(function () {
    if ($(window).scrollTop() > 180) {
        $('body').addClass('scrolled');
    } else {
        $('body').removeClass('scrolled');
    }
});