# Landing page 3 PIXELS

![image info](./assets/images/template/logo.png)
### Server requirements

- PHP v7.2.34-30
- MySql v8.0.29
- Apache2

### Server kernel

- MODX Revolution v2.8.3 stable

    ### Installed packages
  + pdoTools
  + MIGX
  + ms2Gallery
  + ClientConfig
  + Console
  + Ace

### Used plugins

- Boostrap v5.0.2
- jQuery 3.6.0
- Fancybox v4.0.27
- SweetAlert2
- Slick Slider

### Used fonts

- Montserrat Google font

### Connected styles and scripts


- /assets/css/bootstrap.min.css
- /assets/css/sweetalert2.min.css
- /assets/css/fancybox.css
- /assets/css/typography.css
- /assets/css/style.css
- /assets/css/mobile.css


- /assets/js/jquery-3.6.0.min.js
- /assets/js/fancybox.js
- /assets/js/bootstrap.min.js
- /assets/js/sweetalert2.min.js
- /assets/js/script.js

### Typography

- --color-white           : #FFF;
- --color-red             : #F71735;
- --color-blue            : #034488;
- --color-black           : #02111B;
- --color-dark-gray       : #4B4B4B;
- --color-gray-text       : #333333;
- --color-middle-gray     : #707070;
- --color-light-gray      : #D9D9D9;
- --color-lightest-gray   : #F0F0F0;
- --color-white-60        : rgba(255, 255, 255, 0.6);
- --color-gray-10         : rgba(75, 75, 75, 0.1);
- --color-dark-70         : rgba(2, 17, 27, 0.7);
- --color-black-30        : rgba(0, 0, 0, 0.3);
- --color-black-10        : rgba(0, 0, 0, 0.1);


- \* {
  outline: 0 !important;
  }

- body {
  font-family: 'Montserrat', sans-serif;
  font-size: 18px;
  line-height: 22px;
  background: #fff;
}
- section {
  margin: 24px auto 48px auto;
}
- section#section-head {
  margin-top: 0 !important;
}
- h1, h2, h3, h4, h5,g
  .h1, .h2, .h3, .h4, .h5,
  .t1, .t2, .t3, .t4, .t5,
  .b1, .b1-reg, .b2 {
  font-style: normal;
  color: var(--color-gray-text);
}
- h1 {
  font-weight: 700;
  font-size: 96px;
  line-height: 117px;
  text-transform: uppercase;
}
- h2 {
  font-weight: 600;
  font-size: 40px;
  line-height: 49px;
  text-transform: uppercase;
}
- h3 {
  font-weight: 600;
  font-size: 36px;
  line-height: 44px;
}
- h5 {
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  text-transform: uppercase;
}
- .t1, .t1-uppercase {
  font-size: 24px;
  line-height: 29px;
}
- /* Text T1 */
.t1 {
  font-weight: 500;
}
- /* Text T1 Uppercase */
.t1-uppercase {
  text-transform: uppercase;
}
- /* Text T2 */
.t2 {
  font-weight: 400;
  font-size: 22px;
  line-height: 27px;
}
- /* Text T3 */
.t3 {
  font-weight: 400;
  font-size: 18px;
  line-height: 22px;
}
- /* Text T3.1 */
.t3-1 {
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
}
- .t4, .t4-extrabold {
  font-size: 20px;
  line-height: 24px;
}
- /* Text T4 */
.t4 {
  font-weight: 400;
}
- /* Text T4 Extrabold */
.t4-extrabold {
  font-weight: 800;
}
- .btn, .b1, .b1-reg {
font-size: 16px;
  line-height: 20px;
}
- /* Button B1 */
.b1 {
  font-weight: 500;
}
- /* Button B1 reg */
.b1-reg {
  font-weight: 400;
}
- /* Button B2 */
.b2 {
font-size: 14px;
line-height: 17px;
}
- .btn {
  width: auto;
  height: auto;
  min-width: 239px;
  min-height: 56px;
}
- .btn .button__bg, .btn .button__text {
  -webkit-transform: translateZ(0);
  transform: translateZ(0);
}
- .btn .button__text {
  position: relative;
}
- .btn .button__bg{
  background-color: transparent;
  border-radius: 7px;

  _transition: -webkit-transform .3s ease;
  transition: transform .3s ease;
  transition: transform .3s ease,-webkit-transform .3s ease;
  
  -webkit-transform-origin: 50% 50%;
  -ms-transform-origin: 50% 50%;
  transform-origin: 50% 50%;_
}
- .btn {
  position: relative;
}
- .btn, .btn:hover, .btn:active, .btn:focus {
  -webkit-box-shadow: none !important; -moz-box-shadow: none !important; -o-box-shadow: none !important; box-shadow: none !important;
}
- .btn .button_text, .btn .button_text:hover, .btn .button_text:active, .btn .button_text:focus {
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  color: var(--color-black);
}
- .btn .button__bg {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
- .btn:hover .button__bg {
  -webkit-transform: scaleY(1.2);
  -ms-transform: scaleY(1.2);
  transform: scaleY(1.2);
}
- .btn:active .button__bg {
  -webkit-transform: scaleY(1);
  -ms-transform: scaleY(1);
  transform: scaleY(1);
}
- .btn.btn-blue .button__text, .btn.btn-black .button__text {
  color: var(--color-white)
}
- .btn.btn-gray .button__bg, .btn.btn-gray .button__bg:hover, .btn.btn-gray .button__bg:active, .btn.btn-gray .button__bg:focus {
  background: var(--color-lightest-gray);
}
- .btn.btn-blue .button__bg, .btn.btn-blue .button__bg:hover, .btn.btn-blue .button__bg:active, .btn.btn-blue .button__bg:focus {
  background: var(--color-blue);
}
- .btn.btn-black .button__bg, .btn.btn-black .button__bg:hover, .btn.btn-black .button__bg:active, .btn.btn-black .button__bg:focus {
  background: var(--color-black);
}