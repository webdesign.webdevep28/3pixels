<?php
if (!isset($_POST['mailto'])) {
    echo json_encode(['Error' => 'Unknown mailto var']);

    exit;
};

$mailTo = $_POST['mailto'];
$name = $_POST['name'];
$phone = $_POST['phone'];
$email = $_POST['email'];
$capturePoint = $_POST['capturePoint'];
$siteName = $_SERVER['SERVER_NAME'];

$message = "
    <h2>Вы получили заявку на разработку лендинга с сайта {$sitename}</h2>
    <ul>
        <li>Имя: {$name}</li>
        <li>Телефон: {$phone}</li>
        <li>E-mail: {$email}</li>
        <li>Точка завхата: {$capturePoint}</li>
    </ul>
";

$to = $mailto;
$subject = "Заказ на разработку лендинга";
$headers = "From: 3PIXELS<support@{$siteName}>\r\nContent-type: text/html; charset=utf-8 \r\n";

$result = mail($mailTo, $subject, $message, $headers) ? ['Success' => true] : ['Error' => 'Unknown mailto var'];

echo json_encode($result);