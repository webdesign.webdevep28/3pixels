<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

<link rel="stylesheet"  href="/assets/css/bootstrap.min.css">
<link rel="stylesheet"  href="/assets/css/sweetalert2.min.css">
<link rel="stylesheet"  href="/assets/css/fancybox.css">
<link rel="stylesheet"  href="/assets/css/slick-slider.min.css">
<link rel="stylesheet"  href="/assets/css/slick-slider-theme.min.css">

<link rel="stylesheet" href="/assets/css/typography.css?v={'!microtime' | snippet}">
<link rel="stylesheet" href="/assets/css/style.css?v={'!microtime' | snippet}">
<link rel="stylesheet" href="/assets/css/mobile.css?v={'!microtime' | snippet}">

<script src="/assets/js/jquery-3.6.0.min.js"></script>
<script src="/assets/js/fancybox.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/sweetalert2.min.js"></script>
<script src="/assets/js/slick-slider.min.js"></script>
<script src="/assets/js/script.js?v={'!microtime' | snippet}"></script>