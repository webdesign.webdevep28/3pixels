<!doctype html>
<html lang="ru">
<head>
    <title>{$_modx->resource.pagetitle}</title>
    <base href="{$_modx->config.site_url}">
    <meta charset="utd-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <style>
        .hidden, .hidden-image {
            display: none !important;
        }

        #preloader {
            position: fixed;
            left: 0;
            top: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
            background: rgba(255, 255, 255, 0.98);
            z-index: 99999999 !important;
            width: 100%;
            overflow: hidden;
            animation-delay: 1s
        }

        .item-1 {
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: var(--color-blue-preloader-item-1);
            margin: 7px;
            display: flex;
            justify-content: center;
            align-items: center
        }

        @keyframes scale {
            0% {
                transform: scale(1)
            }
            50%, 75% {
                transform: scale(2.5)
            }
            78%, 100% {
                opacity: 0
            }
        }

        .item-1:before {
            content: '';
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: var(--color-blue-preloader-item-before);
            opacity: .7;
            animation: scale 2s infinite cubic-bezier(0, 0, 0.49, 1.02);
            animation-delay: 200ms;
            transition: .5s all ease;
            transform: scale(1)
        }

        .item-2 {
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: var(--color-blue-preloader-item-2);
            margin: 7px;
            display: flex;
            justify-content: center;
            align-items: center
        }

        @keyframes scale {
            0% {
                transform: scale(1)
            }
            50%, 75% {
                transform: scale(2.5)
            }
            78%, 100% {
                opacity: 0
            }
        }

        .item-2:before {
            content: '';
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: var(--color-blue-preloader-item-before);
            opacity: .7;
            animation: scale 2s infinite cubic-bezier(0, 0, 0.49, 1.02);
            animation-delay: 400ms;
            transition: .5s all ease;
            transform: scale(1)
        }

        .item-3 {
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: var(--color-blue-preloader-item-3);
            margin: 7px;
            display: flex;
            justify-content: center;
            align-items: center
        }

        @keyframes scale {
            0% {
                transform: scale(1)
            }
            50%, 75% {
                transform: scale(2.5)
            }
            78%, 100% {
                opacity: 0
            }
        }

        .item-3:before {
            content: '';
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: var(--color-blue-preloader-item-before);
            opacity: .7;
            animation: scale 2s infinite cubic-bezier(0, 0, 0.49, 1.02);
            animation-delay: 600ms;
            transition: .5s all ease;
            transform: scale(1)
        }

        .item-4 {
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: var(--color-blue-preloader-item-4);
            margin: 7px;
            display: flex;
            justify-content: center;
            align-items: center
        }

        @keyframes scale {
            0% {
                transform: scale(1)
            }
            50%, 75% {
                transform: scale(2.5)
            }
            78%, 100% {
                opacity: 0
            }
        }

        .item-4:before {
            content: '';
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: var(--color-blue-preloader-item-before);
            opacity: .7;
            animation: scale 2s infinite cubic-bezier(0, 0, 0.49, 1.02);
            animation-delay: 800ms;
            transition: .5s all ease;
            transform: scale(1)
        }

        .item-5 {
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: var(--color-blue-preloader-item-5);
            margin: 7px;
            display: flex;
            justify-content: center;
            align-items: center
        }

        @keyframes scale {
            0% {
                transform: scale(1)
            }
            50%, 75% {
                transform: scale(2.5)
            }
            78%, 100% {
                opacity: 0
            }
        }

        .item-5:before {
            content: '';
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: var(--color-blue-preloader-item-before);
            opacity: .7;
            animation: scale 2s infinite cubic-bezier(0, 0, 0.49, 1.02);
            animation-delay: 1000ms;
            transition: .5s all ease;
            transform: scale(1)
        }
    </style>

    <script>
        var ready = (callback) => {
            if (document.readyState != "loading") {
                callback();
            } else {
                document.addEventListener("DOMContentLoaded", callback);
            };
        };

        ready(() => {
            document.getElementById('preloader').animate({
                opacity: "0"
            }, 500);
            setTimeout(function () {
                document.getElementById('preloader').remove();
            }, 500);

            if (window.innerWidth < 640) {
                document.querySelector('#section-head img.background').remove();
                document.getElementById('section-head').setAttribute("style", "background-image: url({1|resource:'images_header_bg'}); min-height: 550px; background-position: center; background-size: cover");
            };
        });
    </script>
</head>
<body>
<div id="preloader">
    <div class="item-1"></div>
    <div class="item-2"></div>
    <div class="item-3"></div>
    <div class="item-4"></div>
    <div class="item-5"></div>
</div>

{'mainmenu' | chunk}