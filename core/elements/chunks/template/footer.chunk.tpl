        <footer id="site-footer" class="container-fluid flex-center">
            <div class="inner row">
                <div class="col-12 col-sm-4 site-footer-column">
                    <a class="site-footer-logo" href="{$_modx->config.site_url}"><img src="{1|resource:'logo'}" alt="{$_modx->config.site_name}"></a>
                </div>
                <div class="col-12 col-sm-4 site-footer-column">
                    <ul>
                        <li><a class="beautiful-link site-footer-phone" href="tel:{'!clearPhone' | snippet : ['phone' => 1|resource:'contacts_phone']}">{1|resource:'contacts_phone'}</a></li>
                        <li><a class="beautiful-link site-footer-mail" href="mailto:{1|resource:'contacts_email_show'}">email&nbsp;{1|resource:'contacts_email_show'}</a></li>
                        <li>
                            <a class="beautiful-link site-footer-social" href="{1|resource:'contacts_facebook'}" data-target="blank">facebook</a>
                            &nbsp;&nbsp;&nbsp;
                            <a class="beautiful-link site-footer-social" href="https://t.me/{1|resource:'contacts_telegram'}" data-target="blank">telegram</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-4 site-footer-column">
                    <ul>
                        <li>Индивидуальный предприниматель<br>{1|resource:'contacts_fio'}</li>
                        <li>УНП&nbsp;{1|resource:'contacts_unp'}</li>
                    </ul>
                </div>
            </div>
        </footer>
        
        {'assets' | chunk}

        <form action="#" id="settings" class="hidden">
            <input type="hidden" name="privacy_policy_text" value="{1|resource:'privacy_policy_text'}">
            <input type="hidden" name="mailto" value="{1|resource:'contacts_email_orders'}">
        </form>

        <a id="scroll-to-top"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z"/></svg></a>
    </body>
</html>