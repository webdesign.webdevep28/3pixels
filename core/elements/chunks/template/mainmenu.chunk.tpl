<nav id="mainmenu" class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="row">
        <div class="col-5 col-md-3 mainmenu-column">
            <span class="navbar-brand mainmenu-logo" href="{$_modx->config.site_url}"><img src="{1|resource:'logo'}" alt="{$_modx->config.site_name}"></span>
        </div>
        <div class="col-1 col-md-6 mainmenu-column">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                {set $rows = json_decode(1|resource:'mainmenu', true)}
                {foreach $rows as $row}
                    <li class="nav-item"><a class="nav-link b2" href="#" data-target-id="{$row.section_id}">{$row.title}</a></li>
                {/foreach}
                </ul>
            </div>
        </div>
        
        <div class="col-7 col-md-3 mainmenu-column mainmenu-phone">
            <a href="tel:+{'!clearPhone' | snippet : ['phone' => 1|resource:'contacts_phone']}" class="b2 beautiful-link hide-on-mobile">{1|resource:'contacts_phone'}</a>
            <a href="tel:+{'!clearPhone' | snippet : ['phone' => 1|resource:'contacts_phone']}" class="b2 beautiful-link show-on-mobile">{1|resource:'contacts_phone_mobile_icon'}</a>
        </div>
    </div>
</nav>