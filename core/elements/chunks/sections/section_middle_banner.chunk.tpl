<section id="section-middle-banner" class="container-fluid">
    <img class="background" src="{1|resource:'images_middle_banner_bg'}" alt="{$_modx->resource.pagetitle}">

    <div class="inner block-center container">
        <div class="row">
            <div class="col-12 col-sm-6">
                <h3>{1|resource:'section_middle_banner_h3'}</h3>
                <button class="btn btn-gray make-order show-popover">
                    <span class="button__bg"></span>
                    <span class="button__text">
                        {1|resource:'button_order_name'}
                    </span>
                </button>
            </div>
            <div class="banner-inside-image col-12 col-sm-6">
                <img src="{1|resource:'images_middle_banner_inside'}" alt="{$_modx->resource.pagetitle}">
            </div>
        </div>
    </div>
</section>