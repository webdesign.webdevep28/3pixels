<section id="section-rates" class="container">
    <h2>{1|resource:'section_rates_h2'}</h2>

    <div class="inner row slider single-item">
    {set $rows = json_decode(1|resource:'rates', true)}
    {foreach $rows as $row}
        <div class="rates-item col col-sm-4">
            <div class="inner">
                <h4 class="t1" data-capture-point="Тарифы: {$row.title}">{$row.title}</h4>
                <ul class="t3-1">
                    {$row.items}
                </ul>
                <div class="row">
                    <div class="t4 col align-left">Срок сдачи</div>
                    <div class="t4-extrabold col align-right">{$row.deadline}</div>
                </div>
                <div class="row">
                    <div class="t4 col align-left">Цена</div>
                    <div class="t4-extrabold col align-right">{$row.price} BYN</div>
                </div>
                <div class="row">
                    <button class="btn btn-blue make-order show-popover">
                        <span class="button__bg"></span>
                        <span class="button__text">{1|resource:'button_order_name'}</span>
                    </button>
                </div>
            </div>
        </div>
    {/foreach}
    </div>
</section>