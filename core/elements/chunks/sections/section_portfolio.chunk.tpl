<section id="section-portfolio" class="container">
    <h2>{1|resource:'section_portfolio_h2'}</h2>

    <div class="row image-gallery slider single-item">
        {'!ms2Gallery' | snippet : ['resources' => 1,'tpl' => 'ms2Gallery-portfolio']}
    </div>

    <div class="row">
        <a href="#" class="b1 link-load-more" data-function="show-hidden" data-target-class="hidden-image">Загрузить ещё</a>
    </div>
</section>