<section id="section-faq" class="container">
    <h2>{1|resource:'section_faq_h2'}</h2>

    <div class="inner">
        {set $rows = json_decode(1|resource:'faq', true)}

        {if $rows}
            <ul class="row">
                {foreach $rows as $row}
                    <li class="t2 section-faq-item">
                        <div class="section-faq-item-question">{$row.question}</div>
                        <div class="section-faq-item-answer b1-reg">{$row.answer}</div>
                    </li>
                {/foreach}
            </ul>
        {/if}
    </div>
</section>