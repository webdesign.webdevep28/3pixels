<section id="callback-form-on-page" class="container-fluid flex-center">
    <img class="background" src="{1|resource:'images_callback_form_bg'}" alt="{$_modx->resource.pagetitle}">

    <div class="inner block-center container">
        <h4 class="t1 title">{1|resource:'form_title'}</h4>
        
        <form action="#" id="callback-form" class="callback-form">
            <input type="text" name="name" {if 1|resource:'rules_form_name_required'}minlength="{1|resource:'rules_form_name_min_length'}" required="required" {/if}placeholder="{1|resource:'form_field_name'}">
            <input type="tel" onkeyup="this.value = this.value.replace (/[^0-9+()]/, '')" name="phone" {if 1|resource:'rules_form_phone_required'}minlength="{1|resource:'rules_form_phone_min_length'}" required="required" {/if}placeholder="{1|resource:'form_field_phone'}">
            <input type="email" name="email" {if 1|resource:'rules_form_email_required'}minlength="{1|resource:'rules_form_email_min_length'}" required="required" {/if}placeholder="{1|resource:'form_field_email'}">

            <button type="submit" class="btn btn-black make-order">
                <span class="button__bg"></span>
                <span class="button__text">
                    {1|resource:'button_order_name'}
                </span>
            </button>

            <label class="agreement">{1|resource:'form_field_agreement'}</label>
        </form>
    </div>
</section>