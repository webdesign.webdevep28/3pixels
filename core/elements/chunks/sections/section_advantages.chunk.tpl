<section id="section-advantages" class="container">
    <h2>{1|resource:'section_advantages_h2'}</h2>

    <div class="inner row slider single-item">
    {set $rows = json_decode(1|resource:'advantages', true)}
    {foreach $rows as $row}
        <div class="advantages-item col-12 col-sm-6">
            <div class="inner">
                <div class="row advantages-item-icon">
                    <img src="{$row.icon}" alt="{$row.title}">
                </div>
                <h5>{$row.title}</h5>
                <div class="row advantages-item-text">
                    <p class="t3">{$row.text}</p>
                </div>
            </div>
        </div>
    {/foreach}
    </div>
</section>