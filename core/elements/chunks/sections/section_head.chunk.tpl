<section id="section-head" class="container-fluid">
    <img class="background" src="{1|resource:'images_header_bg'}" alt="{$_modx->resource.pagetitle}">

    <div class="inner block-center container">
        <h1 data-capture-point="Хедер сайта">{1|resource:'section_head_h1'}</h1>
        <h3>{1|resource:'section_head_h3'}</h3>
        <h4>{1|resource:'section_head_h4'}</h4>
        <button class="btn btn-gray make-order show-popover">
            <span class="button__bg"></span>
            <span class="button__text">
                {1|resource:'button_order_name'}
            </span>
        </button>
    </div>
</section>