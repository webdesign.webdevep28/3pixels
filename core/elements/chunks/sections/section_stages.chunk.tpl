<section id="section-stages" class="container">
    <h2>{1|resource:'section_stages_h2'}</h2>

    <div class="inner">
    {set $rows = json_decode(1|resource:'stages', true)}

    {if $rows}
        <ul class="row">
            {set $position = 1}
            {set $default_title = null}
            {set $default_text = null}

            {foreach $rows as $row}
                <li class="b1 col-12 col-sm-2 section-stages-item{if !$default_title} active{/if}" data-position="{$position}">
                    <span class="position">{$position}</span>
                    <span class="title">{$row.title}</span>
                    <span class="hidden text">{$row.description}</span>
                </li>

                {if !$default_title}
                    {set $default_title = $row.title}
                    {set $default_text = $row.description}
                {/if}

                {set $position += 1}
            {/foreach}
        </ul>
        <div class="section-stages-selected-text row">
            <h4 class="title col-12">{$default_title}</h4>
            <p class="text col-12 col-lg-6 t3">{$default_text}</p>
        </div>
    {/if}
    </div>
</section>