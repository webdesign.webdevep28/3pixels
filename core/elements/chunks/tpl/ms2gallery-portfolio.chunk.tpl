{if $files?}
    {set $counter = 0}

    {foreach $files as $file}
        <div class="col-12 col-sm-6 image-gallery-item {if $counter > 5}hidden-image{else}fade-in{/if}">
            <div class="inner">
                <a href="{$file['url']}"
                   data-fancybox="gallery"
                   data-caption="{$file['name']}">
                    <img src="{$file['medium']}" alt="{$file['name']}" title="{$file['name']}">
                </a>
            </div>
        </div>

        {set $counter = $counter + 1}
    {/foreach}
{/if}